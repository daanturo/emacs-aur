#!/usr/bin/env bash

# must be run in Emacs's source directory

dir="$(realpath $(dirname $(realpath $0)))"

set -o pipefail

set -x

### Dependencies

version=$(grep -E '^AC_INIT' configure.ac | grep -oE ',[^,]*?([0-9]+(\.[0-9]+)+)[^,]*,' | grep -oE '[0-9.]+')

major_version="$(pcregrep -o1 '^([0-9]+)' <<<$version)"

#### Fedora family

if command -v dnf &>/dev/null; then
    yum install -y dnf-plugins-core epel-release epel-next-release
    sudo dnf config-manager --set-enabled powertools crb
fi

#### OpenSUSE family

if command -v yum &>/dev/null; then
    sudo yum install -y make gcc autoconf texinfo \
        libX11-devel libXaw-devel \
        libjpeg-turbo-devel libpng-devel libtiff-devel giflib-devel \
        gnutls-devel ncurses-devel \
        libgccjit-devel
fi

#### Arch
function arch_PKBUILD_depends() {
    [[ -d "$1" ]] || return
    cd "$1"
    $2
    source PKGBUILD
    local pkgs="${depends[@]} ${makedepends[@]}"
    # sudo -n -v
    if command -v pacman &>/dev/null && ! pacman -Q $pkgs &>/dev/null; then
        sudo pacman -Syu --noconfirm --needed $pkgs
    fi
    cd -
}

aur_dir=~/.cache/aur/emacs${major_version}-git
if [[ ! -d $aur_dir ]]; then
    git clone https://aur.archlinux.org/emacs${major_version}-git.git $aur_dir
    if (($? != 0)); then
        aur_dir=~/.cache/aur/emacs-git
        git clone https://aur.archlinux.org/emacs-git.git $aur_dir
    fi
fi
arch_PKBUILD_depends $aur_dir "git pull"

# get config_flags
arch_PKBUILD_depends "${dir}/.."

### Initialize

tag="${version}_$(date '+%F_%H-%M-%S')_$(git rev-parse HEAD)_$(git symbolic-ref --short HEAD)"

# export PATH="/usr/lib/ccache/bin/:$PATH"

for arg in "$@"; do
    if [[ $arg =~ "^--prefix=" ]]; then
        prefix_switch="$arg"
    fi
done

# version-time-commit-branch
fall_back_prefix="$HOME/opt/emacs-${tag}"

if [[ -z "$prefix_switch" ]]; then
    prefix_switch="--prefix=${fall_back_prefix}"
fi

# echo "$prefix_switch"

prefix_dir=${prefix_switch/--prefix=/}

[[ -d "$prefix_dir" ]] && chmod -R u+w "$prefix_dir"

_config_flags+=" $prefix_switch $@ "

# # remove overridden flags, but may not be needed?
# for opt in $@; do
#     flag=$(grep -oE '^[^=]+' <<<$opt)
#     _config_flags=$(sed -E "s/ ${flag}[^ ]*//" <<<"$_config_flags")
# done

function install() {

    [[ -f ./autogen.sh ]] && ./autogen.sh
    [[ -f ./autogen ]] && ./autogen
    ./configure $_config_flags

    make -j$(($(nproc) / 2)) install
}

### Build

(
    make clean
    install || (
        rm -rf "$prefix_dir"
        make extraclean
        install
    )
) && (

    ln -sf "$prefix_dir"/bin/emacs-${version} ~/.local/bin-fallback/emacs-"$tag"
    test -f ~/.local/bin-fallback/emacs-${version} || ln -s "$prefix_dir"/bin/emacs-${version} ~/.local/bin-fallback/emacs-${version}

    ln -sf "$prefix_dir"/bin/emacs-${version} ~/.local/bin-fallback/emacs-last-build

    chmod -R a-w ""$prefix_dir""

)
